from ..pipeline1 import *
import pytest
import os
import re
import math

ROOT_DIR = '.'

def delete_task_tag_files(directory):
    for file in os.listdir(directory):
        if re.search('task.*[.]txt', file) is not None:
            os.remove(os.path.join(directory, file))

@pytest.fixture(autouse=True)
def run_around_tests():
    """
    Automatically run before and after each test.
    Remove all files like 'task.*[.]txt'.
    """
    print('before_test')
    delete_task_tag_files(ROOT_DIR)

    # After test
    yield
    print('after test')
    # delete_task_tag_files(ROOT_DIR)

def test_delay_between_files_creation():
    """
    Test a delay between the 2 files creation.
    """
    delay = 1.2
    luigi.build([Task2(delay)], local_scheduler=True)
    assert os.path.getmtime('task2-done-{}.txt'.format(delay)) - \
           os.path.getmtime('task1-done-{}.txt'.format(delay)) >= (math.floor(delay) - 1e-2)


if __name__ == '__main__':
    pytest.main()
