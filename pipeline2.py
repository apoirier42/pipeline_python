"""
Testing Python Luigi package to build pipelines.

2 jobs depending on a single one.

Usage:
    PYTHONPATH='.' luigi --module pipeline2 EndTask --local-scheduler
    PYTHONPATH='.' luigi --module pipeline2 EndTask --param 3 --local-scheduler
    luigi.build([EndTask(3)], local_scheduler=True)
"""

import luigi
import logging
import time

# logging.config.fileConfig('logging.conf')
# logger = logging.getLogger('luigi-interface')


class FetchData(luigi.Task):
    param = luigi.IntParameter(default=41)

    def run(self):
        # logger.debug('FetchData param: {}'.format(self.param))
        f = self.output().open('w')
        f.write('FetchData done.'.format(self.param))
        f.close()

    def output(self):
        return luigi.LocalTarget('task-FetchData-{}-done.txt'.format(self.param))


class RunAlgorithms(luigi.Task):
    param = luigi.IntParameter(default=42)

    def requires(self):
        """
        Depends on this task
        """
        return FetchData(self.param)

    def run(self):
        # logger.debug('task RunAlgorithms param: {}'.format(self.param))

        # create file referenced by output()
        f = self.output().open('w')
        f.write('RunAlgorithms done.'.format(self.param))
        f.close()

    def output(self):
        return luigi.LocalTarget('task-RunAlgorithms-{}-done.txt'.format(self.param))


class ReportA(luigi.Task):
    param = luigi.IntParameter(default=42)

    def requires(self):
        """
        Depends on this task
        """
        return RunAlgorithms(self.param)

    def run(self):
        # logger.debug('ReportA param: {}'.format(self.param))
        time.sleep(1)

        # create file referenced by output()
        f = self.output().open('w')
        f.write('ReportA done.'.format(self.param))
        f.close()

    def output(self):
        return luigi.LocalTarget('task-ReportA-{}-done.txt'.format(self.param))


class ReportB(luigi.Task):
    param = luigi.IntParameter(default=42)

    def requires(self):
        """
        Depends on this task
        """
        return RunAlgorithms(self.param)

    def run(self):
        # logger.debug('ReportB param: {}'.format(self.param))
        time.sleep(2)

        # create file referenced by output()
        f = self.output().open('w')
        f.write('ReportB done.'.format(self.param))
        f.close()

    def output(self):
        return luigi.LocalTarget('task-ReportB-{}-done.txt'.format(self.param))


class EndTask(luigi.Task):
    """
    Final single point to call.
    """
    param = luigi.IntParameter(default=42)

    def requires(self):
        """
        Depends on many tasks
        """
        return [ReportA(self.param), ReportB(self.param)]

    def run(self):
        # logger.debug('Final param: {}'.format(self.param))

        # create file referenced by output()
        f = self.output().open('w')
        f.write('Final done.'.format(self.param))
        f.close()

    def output(self):
        return luigi.LocalTarget('task-Final-{}-done.txt'.format(self.param))


if __name__ == '__main__':
    luigi.build([EndTask(2)], local_scheduler=True)
