"""
Testing Python Luigi package to build pipelines.

Usage:
    PYTHONPATH='.' luigi --module pipeline1 Task2 --local-scheduler
    PYTHONPATH='.' luigi --module pipeline1 Task2 --param 3 --local-scheduler
    luigi.build([Task2(3)], local_scheduler=True)
"""

import luigi
import time
import logging

# logging.config.fileConfig('logging.conf')
# logger = logging.getLogger('luigi-interface')


class Task1(luigi.Task):
    param = luigi.FloatParameter(default=41)

    def run(self):
        # logger.debug('task 1 param: {}'.format(self.param))

        # create file referenced by output()
        f = self.output().open('w')
        f.write('Hello world\n{}\nTask 1 done.'.format(self.param))
        f.close()

    def output(self):
        return luigi.LocalTarget('task1-done-{}.txt'.format(self.param))


class Task2(luigi.Task):
    param = luigi.FloatParameter(default=42)

    def requires(self):
        """
        Depends on this task
        """
        return Task1(self.param)

    def run(self):
        # logger.debug('task 2 param: {}'.format(self.param))

        if self.param < 5:
            time.sleep(self.param)

        # create file referenced by output()
        f = self.output().open('w')
        f.write('\nTask 2 done.\n')
        f.close()

    def output(self):
        return luigi.LocalTarget('task2-done-{}.txt'.format(self.param))


if __name__ == '__main__':
    luigi.build([Task2(1)], local_scheduler=True)
